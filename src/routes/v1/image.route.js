const express = require('express');

var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const itemValidation = require('../../validations/item.validation');
const imageController = require('../../controllers/image.controller');

const router = express.Router();

router
  .route('/')
  .post(upload.single('fileData'), imageController.saveImage)
  // .get(auth('getItems'), validate(itemValidation.getItems), itemController.getItems);


module.exports = router;