const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const productValidation = require('../../validations/product.validation');
const productController = require('../../controllers/product.controller');

const router = express.Router();

router
  .route('/')
  .post(auth('manageItems'), validate(productValidation.createProduct), productController.createProduct)
  .get(auth('getItems'), validate(productValidation.getProducts), productController.getProducts);

router
  .route('/inventory')
  .get(auth('getInventoryItems'), validate(productValidation.getProducts), productController.getInventoryProducts);

router
  .route('/:productId')
  .get(auth('getItems'), validate(productValidation.getProduct), productController.getProduct)
  .patch(auth('manageItems'), validate(productValidation.getProduct), productController.updateProduct)
  .delete(auth('manageItems'), validate(productValidation.getProduct), productController.deleteProduct);

module.exports = router;