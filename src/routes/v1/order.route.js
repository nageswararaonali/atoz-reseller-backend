const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const orderValidation = require('../../validations/order.validation');
const orderController = require('../../controllers/order.controller');

const router = express.Router();

router
  .route('/')
  .get(auth('manageOrder'), validate(orderValidation.getOrders), orderController.getOrders)
  .post(auth('placeOrder'), validate(orderValidation.createOrder), orderController.createOrder);

router
  .route('/test')
  .get(validate(orderValidation.test), orderController.test);

router
  .route('/processOrder/:orderId')
  .get(auth('manageOrder'), validate(orderValidation.processOrder), orderController.processOrder);

router
  .route('/:orderId')
  .delete(auth('manageOrder'), validate(orderValidation.getOrder), orderController.deleteOrder);
module.exports = router;
