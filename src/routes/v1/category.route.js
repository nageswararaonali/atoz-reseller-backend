const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const categoryValidation = require('../../validations/category.validation');
const categoryController = require('../../controllers/category.controller');

const router = express.Router();

router
  .route('/')
  .post(auth('manageCategories'), validate(categoryValidation.createCategory), categoryController.createCategory)
  .get(auth('getCategories'), validate(categoryValidation.getCategories), categoryController.getCategories);

router
  .route('/get-all')
  .get(auth('getCategories'), validate(categoryValidation.getCategories), categoryController.getCategoriesProducts);

router
  .route('/:categoryId')
  .get(auth('getCategories'), validate(categoryValidation.getCategory), categoryController.getCategory)
  .patch(auth('manageCategories'), validate(categoryValidation.getCategory), categoryController.updateCategory)
  .delete(auth('manageCategories'), validate(categoryValidation.getCategory), categoryController.deleteCategory);


module.exports = router;
