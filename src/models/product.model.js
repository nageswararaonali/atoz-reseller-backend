const mongoose = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('./plugins');

const productSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    price: {
      type: Number,
      trim: true,
    },
    image: {
      type: String,
      trim: true,
      lowercase: true
    },
    displayImage: {
      type: String,
      trim: true,
      lowercase: true
    },
    mainImage: {
      type: String,
      trim: true,
      lowercase: true
    },
    isSingle: {
      type: Boolean,
      default: true
    },
    items: {
      type: Array,
      default: []
    },
    quantity: {
      type: Number,
      default: 0
    },
    categoryId: {
      type: String,
      required: true
    },
    status: {
      type: String,
      enum: ['Active', 'Inactive'],
      default: 'Active',
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
productSchema.plugin(toJSON);
productSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
productSchema.statics.isNameTaken = async function (name, excludeProductId) {
  const product = await this.findOne({ name, _id: { $ne: excludeProductId } });
  return !!product;
};

/**
 * @typedef User
 */
const Product = mongoose.model('Products', productSchema);

module.exports = Product;
