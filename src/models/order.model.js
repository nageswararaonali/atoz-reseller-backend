const mongoose = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('./plugins');

const orderSchema = mongoose.Schema(
  {
    mobile: {
      type: String,
      required: true,
      trim: true,
    },
    name: {
      type: String,
      trim: true,
    },
    shopName: {
      type: String,
      trim: true,
    },
    city: {
      type: String,
      trim: true,
    },
    pdfUrl: {
      type: String,
      trim: true,
    },
    totalItems: {
      type: Number
    },
    totalValue: {
      type: Number
    },
    orderNumber: {
      type: Number
    },
    status: {
      type: String,
      enum: ['Active', 'Inactive', 'Hold', 'Processed'],
      default: 'Active',
    },
    orderData: {
      type: Array,
      default: []
    }
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
orderSchema.plugin(toJSON);
orderSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
orderSchema.statics.isNameTaken = async function (name, excludeItemId) {
  const item = await this.findOne({ name, _id: { $ne: excludeItemId } });
  return !!item;
};

/**
 * @typedef User
 */
const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
