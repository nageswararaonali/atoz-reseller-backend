const mongoose = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('./plugins');

const ordernumberSchema = mongoose.Schema(
  {
    number: {
      type: Number
    }
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
ordernumberSchema.plugin(toJSON);
ordernumberSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
ordernumberSchema.statics.isNameTaken = async function (name, excludeItemId) {
  const item = await this.findOne({ name, _id: { $ne: excludeItemId } });
  return !!item;
};

/**
 * @typedef User
 */
const Ordernumber = mongoose.model('Ordernumber', ordernumberSchema);

module.exports = Ordernumber;
