const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { productService, orderService } = require('../services');
var pdf = require("pdf-creator-node");
var fs = require("fs");
var path = require('path');
const AWS = require('aws-sdk');

const test = catchAsync(async(req, res) => {
  let num = 0;
  const ordern = await orderService.getOrderNumber();
  console.log(ordern)
  if(ordern) {
    num = ordern.number + 1
  } else {
    num = 1
  }
  const ordernu = orderService.updateOrderNumber(num);
})
const createOrder = catchAsync(async (req, res) => {
  console.log(req.body.cart)
  console.log(req.user)
  let cartData = req.body.cart;
  const mobile = req.user.mobile;
  const name = req.user.name;
  const email = req.user.email;
  const city = req.user.city;
  const shopName = req.user.shopName;
  let totalItems = 0
  let totalValue = 0
  cartData = cartData.map((cData, index) => {
    cData['sno'] = index+1
    totalItems = totalItems + cData.value
    totalValue = totalValue + (cData.value * cData.price)
    return cData
  })
  let num = 0;
  const ordern = await orderService.getOrderNumber();
  if(ordern) {
    num = ordern.number + 1
  } else {
    num = 1
  }
  const ordernu = orderService.updateOrderNumber(num);
  var html = fs.readFileSync(path.join("src/docs", "template.html"), "utf8");
  var options = {
      format: "A3",
      orientation: "portrait",
      border: "10mm",
      header: {
          height: "45mm",
          contents: '<div style="text-align: center;">A2Z</div>'
      },
      footer: {
          height: "28mm",
          contents: {
              first: 'Order',
              2: 'Second page', // Any page number is working. 1-based index
              default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
              last: 'Last Page'
          }
      }
  };
  var pdfDocument = {
      html: html,
      data: {
        products: cartData,
        name,
        mobile,
        email,
        totalItems,
        totalValue,
        shopName,
        city, 
        num
      },
      path: path.join("uploads", "output.pdf"),
      type: "",
    };
    pdf
    .create(pdfDocument, options)
    .then((res1) => {
      console.log(res1);
      const s3 = new AWS.S3({
          accessKeyId: process.env.ACCESSKEYID,
        secretAccessKey: process.env.SECRETACCESSKEY
        });
      const timeStr = new Date().toISOString()
      var params = {
        Bucket: process.env.BUCKET_NAME,
        Body: fs.createReadStream(path.join("uploads", "output.pdf")),
        Key: `product/orders/${mobile}/${timeStr}.pdf`
      };
      s3.upload(params, (err, data) => {
        if (err) {
          console.log('Error occured while trying to upload to S3 bucket', err);
        }

        if (data) {
          fs.unlinkSync(path.join("uploads", "output.pdf")); // Empty temp folder
          const locationUrl = data.Location;
          const orderDataPayload = {
            mobile,
            orderData: cartData,
            status: 'Active',
            pdfUrl: locationUrl,
            totalItems,
            totalValue,
            name,
            shopName,
            orderNumber: num
          }
          const order = orderService.createOrder(orderDataPayload);
  
          res.status(httpStatus.CREATED).send({success: true, pdfurl: locationUrl});
          // res.status(httpStatus.CREATED).send({imageUrl: locationUrl, imageType: req.body.imageType});
        }
      });
    })
    .catch((error) => {
      console.error(error);
      res.status(httpStatus.CREATED).send({success: false, message: 'Problem in uploading pdf'});
    });
  /*let cartData = req.body.cart;
  let productIds = cartData.map((product) => {
    return product.productId
  })
  console.log(productIds)
  const productsQuery = await productService.queryProductsByIds(productIds, {});
  // console.log(item)
  let products = productsQuery.results
  let isSuccess = true;
  for await (const cartD of cartData){
    products.map((product) => {
      if(product.id == cartD.productId) {
        product.items.map((item) => {
          if(item.quantity < cartD.value) {
            isSuccess = false
          }
        })
      }
    })
  }
  if(isSuccess) {
    let i = 1;
    for await (const cartD of cartData){
      for await (const product of products) {
        if(cartD.productId == product.id) {
          await productService.updateProduct({$and: [{_id: product.id}, {"items.item.id": cartD.itemId}]}, {$inc:{"items.$.quantity": -cartD.value}})
        }
      }
      cartD['sno'] = i;
      i++;
    }
    console.log(path.resolve())
    var html = fs.readFileSync(path.join("src/docs", "template.html"), "utf8");
    var options = {
        format: "A3",
        orientation: "portrait",
        border: "10mm",
        header: {
            height: "45mm",
            contents: '<div style="text-align: center;">A2Z</div>'
        },
        footer: {
            height: "28mm",
            contents: {
                first: 'Order',
                2: 'Second page', // Any page number is working. 1-based index
                default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
                last: 'Last Page'
            }
        }
    };
    console.log("cartData")
    console.log(cartData)
    var pdfDocument = {
      html: html,
      data: {
        products: cartData,
        name: 'Nag'
      },
      path: path.join("uploads", "output.pdf"),
      type: "",
    };
    pdf
    .create(pdfDocument, options)
    .then((res1) => {
      console.log(res1);
      const s3 = new AWS.S3({
          accessKeyId: process.env.ACCESSKEYID,
        secretAccessKey: process.env.SECRETACCESSKEY
        });
      const timeStr = new Date().toISOString()
      var params = {
        Bucket: process.env.BUCKET_NAME,
        Body: fs.createReadStream(path.join("uploads", "output.pdf")),
        Key: `product/orders/${timeStr}.pdf`
      };
      s3.upload(params, (err, data) => {
        if (err) {
          console.log('Error occured while trying to upload to S3 bucket', err);
        }

        if (data) {
          fs.unlinkSync(path.join("uploads", "output.pdf")); // Empty temp folder
          const locationUrl = data.Location;
          res.status(httpStatus.CREATED).send({success: true, pdfurl: locationUrl});
          // res.status(httpStatus.CREATED).send({imageUrl: locationUrl, imageType: req.body.imageType});
        }
      });
    })
    .catch((error) => {
      console.error(error);
      res.status(httpStatus.CREATED).send({success: false, message: 'Problem in uploading pdf'});
    });
    
  } else {
    res.status(httpStatus.CREATED).send({success: false, message: 'Quantity not matched'});
  }*/
  
});

const processOrder = catchAsync(async (req, res) => {
  console.log(req.params.orderId)
  const orderId = req.params.orderId;
  console.log(req.user)
  const order = await orderService.getOrderById(orderId);
  console.log(order)
  
  let cartData = order.orderData;
  let productIds = cartData.map((product) => {
    return product.productId
  })
  console.log(productIds)
  const productsQuery = await productService.queryProductsByIds(productIds, {});
  // console.log(item)
  let products = productsQuery.results
  let isSuccess = true;
  for await (const cartD of cartData){
    products.map((product) => {
      if(product.id == cartD.productId) {
        product.items.map((item) => {
          if(item.quantity < cartD.value) {
            isSuccess = false
          }
        })
      }
    })
  }
  if(isSuccess) {
    for await (const cartD of cartData){
      for await (const product of products) {
        if(cartD.productId == product.id) {
          await productService.updateProduct({$and: [{_id: product.id}, {"items.item.id": cartD.itemId}]}, {$inc:{"items.$.quantity": -cartD.value}})
        }
      }
    }
    console.log("order update calling ...")
    const order1 = await orderService.updateOrder(orderId, {status: 'Processed'});
    res.status(httpStatus.CREATED).send({success: true, message: 'Order Updated'});
  } else {
    res.status(httpStatus.CREATED).send({success: false, message: 'Quantity not matched'});
  }
  
});


const getOrders = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['status']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await orderService.queryOrders(filter, options);
  res.json({data: result, success: true});
});

const deleteOrder = catchAsync(async (req, res) => {
  const orderId = req.params.orderId
  const order1 = await orderService.updateOrder(orderId, {status: 'Inactive'});
  res.json({success: true});
});



module.exports = {
  createOrder,
  processOrder,
  getOrders,
  deleteOrder,
  test
};
