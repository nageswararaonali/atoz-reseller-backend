const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { categoryService, productService } = require('../services');

const createCategory = catchAsync(async (req, res) => {
  const category = await categoryService.createCategory(req.body);
  res.status(httpStatus.CREATED).send(category);
});

const getCategories = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['isCategory', 'parentId']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await categoryService.queryCategories(filter, options);
  res.json({data: result, success: true});
});

const getCategory = catchAsync(async (req, res) => {
  const category = await categoryService.getCategoryById(req.params.categoryId);
  if (!category) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Category not found');
  }
  res.send(category);
});

const updateCategory = catchAsync(async (req, res) => {
  const user = await categoryService.updateCategoryById(req.params.categoryId, req.body);
  res.send(user);
});

const deleteCategory = catchAsync(async (req, res) => {
  await categoryService.deleteCategoryById(req.params.categoryId);
  res.status(httpStatus.NO_CONTENT).send();
});

const getCategoriesProducts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['isCategory', 'parentId']);
  const categoriesRes = await categoryService.queryCategories(filter, {});
  let categories = categoriesRes.results
  let categoriesData = []
  const category = await categoryService.getCategoryById(req.query.parentId);
  
  /*let cats = await Promise.all(categories.filter(async (category) => {
    const productRes = await productService.queryProducts({categoryId: category.id}, {sortBy: 'createdAt:desc'});
    console.log(productRes)
    let catData = {
      id: category._id,
      name: category.name,
      image: category.image,
      products: productRes.results
    }
    categoriesData.push(catData)
    // let cData = {products: productRes.results}
    // category['products'] = productRes.results
    // Object.assign(cData, category._doc)
    // console.log(category)
    console.log(catData)

    return category
  }));*/
  let catIds = categories.map((cat) => {
    return cat.id
  })
  const newProductRes = await productService.queryProducts({categoryId: {$in: catIds}}, {sortBy: 'createdAt:desc', limit: 20});
  let catData = {
    id: 'new_arrivals',
    name: 'New Arrivals',
    image: category.image,
    products: newProductRes.results
  }
  categoriesData.push(catData)
  for await (const category of categories) {
    
    const productRes = await productService.queryProducts({categoryId: category.id}, {sortBy: 'createdAt:desc'});
    // console.log(productRes)
    let catData = {
      id: category._id,
      name: category.name,
      image: category.image,
      products: productRes.results
    }
    categoriesData.push(catData)
    // doc['products'] = productRes.results
    // console.log(category);
  }
  // console.log("cats data ", categoriesData)
  res.send({results:categoriesData})
});


module.exports = {
  createCategory,
  getCategories,
  getCategory,
  updateCategory,
  deleteCategory,
  getCategoriesProducts
};
