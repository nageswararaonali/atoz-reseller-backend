const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { itemService } = require('../services');
var fs = require('fs');
const AWS = require('aws-sdk');

const saveImage = catchAsync(async (req, res) => {
  // const item = await itemService.createItem(req.body);
  console.log("image upload req.body")
  console.log(req.body)
    // aws.config.setPromisesDependency();
    console.log(process.env.REGION)
    const s3 = new AWS.S3({
        accessKeyId: process.env.ACCESSKEYID,
      secretAccessKey: process.env.SECRETACCESSKEY
      });
    var params = {
      Bucket: process.env.BUCKET_NAME,
      Body: fs.createReadStream(req.file.path),
      Key: `product/${req.body.imageType}/${req.file.originalname}`
    };
console.log(params)
    s3.upload(params, (err, data) => {
      if (err) {
        console.log('Error occured while trying to upload to S3 bucket', err);
      }

      if (data) {
        fs.unlinkSync(req.file.path); // Empty temp folder
        const locationUrl = data.Location;
        res.status(httpStatus.CREATED).send({imageUrl: locationUrl, imageType: req.body.imageType});
      }
    });
  
});

const getItems = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'status']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await itemService.queryItems(filter, options);
  res.json({data: result, success: true});
});

const getItem = catchAsync(async (req, res) => {
  const item = await itemService.getCategoryById(req.params.itemId);
  if (!item) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Category not found');
  }
  res.send(item);
});


module.exports = {
  saveImage,
  getItems,
  getItem
};
