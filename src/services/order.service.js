const httpStatus = require('http-status');
const { Order, Ordernumber } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createOrder = async (itemBody) => {
  const order = await Order.create(itemBody);
  console.log(order)
  console.log("order")
  return order;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryOrders = async (filter, options) => {

  const orders = await Order.paginate(filter, options);
  return orders;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getOrderById = async (id) => {
  return Order.findById(id);
};

/**
 * Get user by mobile
 * @param {string} mobile
 * @returns {Promise<User>}
 */
const getUserByMobile = async (mobile) => {
  return Order.findOne({ mobile });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateOrder = async (orderId, updateBody) => {
  const order = await getOrderById(orderId);
  if (!order) {
    throw new ApiError(httpStatus.NOT_FOUND, 'order not found');
  }
  Object.assign(order, updateBody);
  console.log("updateing order")
  console.log(order)
  await order.save();
  return order;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteOrder = async (itemId) => {
  const item = await getOrderById(itemId);
  if (!item) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await item.remove();
  return item;
};

const getOrderNumber = async () => {
  let onumber = await Ordernumber.findOne();
  console.log(onumber)
  return onumber
};

const updateOrderNumber = async (num) => {
  let onumber = await Ordernumber.update({}, {$set:{number: num}}, {upsert: true});
  console.log(onumber)
  return onumber
};
module.exports = {
  createOrder,
  updateOrder,
  getOrderById,
  deleteOrder,
  queryOrders,
  getOrderNumber,
  updateOrderNumber
};
