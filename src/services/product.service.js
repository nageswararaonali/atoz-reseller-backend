const httpStatus = require('http-status');
const { Product } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createProduct = async (productBody) => {
  if (await Product.isNameTaken(productBody.name)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Product Name already taken');
  }
  const product = await Product.create(productBody);
  return product;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryProducts = async (filter, options) => {
  console.log(filter)
  let query = {
    $and: [{"items.quantity": {$gt: 0}}, filter]
  }
  console.log(query)
  console.log(options)
  const products = await Product.paginate(query, options);
  return products;
};

const queryInventoryProducts = async (filter, options) => {
  const products = await Product.paginate(filter, options);
  return products;
};


const queryProductsByIds = async (productIds, options) => {
  let query = {
    _id: {$in: productIds}
  }
  console.log(query)
  console.log(options)
  const products = await Product.paginate(query, options);
  return products;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getProductById = async (id) => {
  return Product.findById(id);
};

/**
 * Get user by mobile
 * @param {string} mobile
 * @returns {Promise<User>}
 */
const getUserByMobile = async (mobile) => {
  return Product.findOne({ mobile });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateProductById = async (productId, updateBody) => {
  const product = await getProductById(productId);
  if (!product) {
    throw new ApiError(httpStatus.NOT_FOUND, 'product not found');
  }
  Object.assign(product, updateBody);
  await product.save();
  return product;
};

const updateProduct = async (query, updateData) => {
  console.log(query)
  console.log(updateData)

  const prd = Product.updateOne(query, updateData)
  console.log(prd)
  return prd
};
/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteProductById = async (productId) => {
  const product = await getProductById(productId);
  if (!product) {
    throw new ApiError(httpStatus.NOT_FOUND, 'product not found');
  }
  await product.remove();
  return product;
};

module.exports = {
  createProduct,
  queryProducts,
  getProductById,
  getUserByMobile,
  updateProductById,
  deleteProductById,
  queryProductsByIds,
  updateProduct,
  queryInventoryProducts
};
