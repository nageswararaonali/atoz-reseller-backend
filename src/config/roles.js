const roles = ['user', 'admin'];

const roleRights = new Map();
roleRights.set(roles[0], ['getCategories', 'placeOrder']);
roleRights.set(roles[1], ['getUsers', 'manageUsers', 'manageCategories', 'getCategories', 'manageItems', 'getItems',
							'manageOrder', 'placeOrder', 'getInventoryItems']);

module.exports = {
  roles,
  roleRights,
};
