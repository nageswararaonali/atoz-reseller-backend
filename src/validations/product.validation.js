const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const createProduct = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    status: Joi.string(),
    image: Joi.string(),
    displayImage: Joi.string().allow(null, ''),
    mainImage: Joi.string().allow(null, ''),
    isSingle: Joi.boolean(),
    items: Joi.array(),
    quantity: Joi.number(),
    price: Joi.number(),
    categoryId: Joi.string()
  }),
};
const getProducts = {
  query: Joi.object().keys({
    name: Joi.string(),
    categoryId: Joi.string(),
    status: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getProduct = {
  params: Joi.object().keys({
    productId: Joi.string()
  }),
};

module.exports = {
  createProduct,
  getProducts,
  getProduct
};
