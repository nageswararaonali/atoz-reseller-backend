
const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const createOrder = {
  body: Joi.object().keys({
    cart: Joi.array()
  }),
};

const getOrders = {
  query: Joi.object().keys({
    status: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getOrder = {
  params: Joi.object().keys({
    orderId: Joi.string()
  }),
};
const test = {
  params: Joi.object().keys({
  }),
};

module.exports = {
  createOrder,
  getOrders,
  getOrder,
  test
};
