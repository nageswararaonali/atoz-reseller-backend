module.exports.authValidation = require('./auth.validation');
module.exports.userValidation = require('./user.validation');
module.exports.categoryValidation = require('./category.validation');
module.exports.itemValidation = require('./item.validation');
module.exports.productValidation = require('./product.validation');
