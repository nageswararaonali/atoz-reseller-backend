const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const createCategory = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    status: Joi.string().required(),
    image: Joi.string(),
    isCategory: Joi.boolean(),
    parentId: Joi.string()
  }),
};
const getCategories = {
  query: Joi.object().keys({
    isCategory: Joi.boolean(),
    parentId: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getCategory = {
  params: Joi.object().keys({
    categoryId: Joi.string()
  }),
};

module.exports = {
  createCategory,
  getCategories,
  getCategory
};
